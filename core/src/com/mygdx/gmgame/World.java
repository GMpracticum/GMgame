package com.mygdx.gmgame;

import com.badlogic.gdx.utils.TimeUtils;
import org.usb4java.Device;
import java.nio.*;
import java.util.*;
import org.usb4java.*;
import practicum.McuBoard;
import practicum.McuWithPeriBoard;

public class World{
	private MyGame myGame;
	public Map map;
	private Player1 player1;
	private Player2 player2;
	private Gun1 gun1;
	private Gun2 gun2;
	private Bullet1 bullet1;
	private Bullet2 bullet2;
	private StatusBar statusBar;
	static long startTime = TimeUtils.millis();
	static long runTime = TimeUtils.timeSinceMillis(startTime);
	
	private McuWithPeriBoard peri;
	
	World (MyGame myGame){
		McuBoard.initUsb();
		Device[] devices = McuBoard.findBoards();
		if (devices.length == 0) {
            System.out.format("** Practicum board not found **\n");
            return;
    	}
    	else {
            System.out.format("** Found %d practicum board(s) **\n", devices.length);
    	}
        McuWithPeriBoard peri = new McuWithPeriBoard(devices[0]);
        
		this.myGame = myGame;
		map = new Map();
		player1 = new Player1(60, 60, this);
		player2 = new Player2(500, 60, this);
		gun1 = new Gun1();
		gun2 = new Gun2();
		bullet1 = new Bullet1(this);
		bullet2 = new Bullet2(this);
		statusBar = new StatusBar(this);
	}
	
	public void update(float delta){
		player1.update();
		player2.update();
		gun1.update();
		gun2.update();
		bullet1.update();
		bullet2.update();
		statusBar.update();
		//runTime = TimeUtils.timeSinceMillis(startTime);
		//System.out.println(runTime);
	}
	
	public MyGame getMyGame(){
		return myGame;
	}
	
	public Map getMap(){
		return map;
	}	
	
	public Player1 getPlayer1(){
		return player1;
	}
	
	public Player2 getPlayer2(){
		return player2;
	}
	
	public Gun1 getGun1(){
		return gun1;
	}
	
	public Gun2 getGun2(){
		return gun2;
	}
	
	public Bullet1 getBullet1(){
		return bullet1;
	}
	
	public Bullet2 getBullet2(){
		return bullet2;
	}
	
	public StatusBar getStatusBar(){
		return statusBar;
	}
	
	public McuWithPeriBoard getPeri(){
		return peri;
	}
}