package com.mygdx.gmgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class Gun1 extends Gun {
	public void update(){
		if(Gdx.input.isKeyPressed(Keys.K)){
			increaseDegree(Gun.rotateSpeed);
    	}
		if(Gdx.input.isKeyPressed(Keys.L)){
			decreaseDegree(Gun.rotateSpeed);
    	}
	}
}