package com.mygdx.gmgame;

import com.badlogic.gdx.math.Vector2;

public class Player {
	public static final int RIGHT = 11;
	public static final int LEFT = 12;
	protected World world;
	protected Map map;
    protected Vector2 position;
    protected float speed = 10;
    protected int status = RIGHT;
    protected float VySpeed;
    protected float gravity = (float) 0.9;
    
    public Vector2 getPosition() {
        return position;    
    }
    
    public int getStatus(){
    	return status;
    }
    
    protected int getRow() {
        return ((int)position.y) / WorldRenderer.BLOCK_SIZE; 
    }
    protected int getColumn() {
        return ((int)position.x) / WorldRenderer.BLOCK_SIZE; 
    }
    
    public boolean canMoveInDirection() {
    	map = world.getMap();
    	if(status == LEFT){
    		return !world.map.hasWallAt(getRow(),getColumn()-1);
    	} else if(status == RIGHT) {
    		return !world.map.hasWallAt(getRow(),getColumn()+1);
    	} else {
    		return true;
    	}
    }
    public void onGravity() {
    	map = world.getMap();
    	if(!world.map.onFloor(getRow(),getColumn())) {
    		position.y = position.y + VySpeed/2;
    		VySpeed += gravity;
    	} else {
    		VySpeed = 0;
    	}
    }
    
    public void onJump() {
    	map = world.getMap();
    	if(world.map.onFloor(getRow(),getColumn())) {
    			VySpeed = -20;
    			position.y = position.y + VySpeed/2;
    			VySpeed += gravity;
    	}
    }
}