package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BulletRenderer {
	private World world;
	private SpriteBatch batch;
	private Texture bullet = new Texture("bulletRight40.png");
	private Bullet1 bullet1;
	private Bullet2 bullet2;
	
	public BulletRenderer(World world){
		this.world = world;
		this.batch = world.getMyGame().batch;
		this.bullet1 = world.getBullet1();
		this.bullet2 = world.getBullet2();
	}
	
	int x1, y1, x2, y2;
	public void render(float delta){
		if(bullet1.isVisible) {
			x1 = (int) world.getBullet1().getPosition().x;
			y1 = (int) world.getBullet1().getPosition().y;
			batch.draw(bullet, x1, MyGame.HEIGHT - y1 - 40);
		}
		if(bullet2.isVisible) {
			x2 = (int) world.getBullet2().getPosition().x;
			y2 = (int) world.getBullet2().getPosition().y;
			batch.draw(bullet, x2, MyGame.HEIGHT - y2 - 40);
		}
	}
}