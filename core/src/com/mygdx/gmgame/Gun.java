package com.mygdx.gmgame;

import com.badlogic.gdx.math.Vector2;

public class Gun {
	protected int degree = 0;
	protected Vector2 position = new Vector2(0, 0);
	protected static int rotateSpeed = 3;

	public void increaseDegree(int value){
		degree = (degree + value) % 360;
	}
	
	public void decreaseDegree(int value){
		degree = (360 + degree - value) % 360;
	}
	
	public int getDegree(){
		return degree;
	}
	
	public void setPosition(float x, float y){
		position.x = x;
		position.y = y;
	}
	
	public Vector2 getPosition(){
		return position;
	}
}
