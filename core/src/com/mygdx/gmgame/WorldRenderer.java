package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WorldRenderer {
	private World world;
	private SpriteBatch batch;
	private MapRenderer mapRenderer;
	private PlayerRenderer playerRenderer;
	private GunRenderer gunRenderer;
	private BulletRenderer bulletRenderer;
	private StatusBarRenderer statusBarRenderer;
	public static final int BLOCK_SIZE = 20;
	
	public WorldRenderer(World world, MapRenderer mapRenderer, PlayerRenderer playerRenderer, GunRenderer gunRenderer, BulletRenderer bulletRenderer, StatusBarRenderer statusBarRenderer) {
		this.world = world;
		this.batch = world.getMyGame().batch;
		this.mapRenderer = mapRenderer;
		this.playerRenderer = playerRenderer;
		this.gunRenderer = gunRenderer;
		this.bulletRenderer = bulletRenderer;
		this.statusBarRenderer = statusBarRenderer;
	}
	
	public void render(float delta) {
		batch.begin();
		mapRenderer.render(delta);
		playerRenderer.render(delta);
		gunRenderer.render(delta);
		bulletRenderer.render(delta);
		statusBarRenderer.render(delta);
		batch.end();
	}
}