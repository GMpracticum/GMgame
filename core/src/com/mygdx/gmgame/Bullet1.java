package com.mygdx.gmgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class Bullet1 extends Bullet {
	
	public Bullet1(World world) {
		super(world);
	}
	
	private void fire(){
		if(Gdx.input.isKeyPressed(Keys.SPACE)){
			if(!isPressFire){
				speed = 0;
				isPressFire = true;
			} else {
				if(speed < maxSpeed)
					speed += 0.6;
			}
		} else if(!fired) {
			if(isPressFire){
				fired = true;
				world.getStatusBar().charge1 = 0;
				isPressFire = false;
				isVisible = true;
				world.getStatusBar().playerTurn = StatusBar.PLAYER1;
				if(world.getPlayer1().status == Player.RIGHT)
					degree = (float) (getInverseDegree(world.getGun1().getDegree()) + 90);
				else
					degree = (float) (world.getGun1().getDegree() - 90);
				double angleInRadian = Math.toRadians(degree);
				double sin = Math.sin(angleInRadian);
				double cos = Math.cos(angleInRadian);
				
				position.x = world.getGun1().getPosition().x;
				position.y = world.getGun1().getPosition().y;
				VySpeed = (float) (speed * cos);
				VxSpeed = (float) (speed * sin);
			}
		}
	}
	
	public void update(){
		fire();
		move();
		bulletHit();
	}
}
