package com.mygdx.gmgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class Bullet2 extends Bullet {
	public Bullet2(World world) {
		super(world);
	}
	
	private void fire(){
		if(Gdx.input.isKeyPressed(Keys.ENTER)){
			if(!isPressFire){
				speed = 0;
				isPressFire = true;
			} else {
				if(speed < maxSpeed)
					speed += 0.5;
			}
		} else if(!fired) {
			if(isPressFire){
				fired = true;
				world.getStatusBar().charge2 = 0;
				isPressFire = false;
				isVisible = true;
				world.getStatusBar().playerTurn = StatusBar.PLAYER2;
				if(world.getPlayer2().status == Player.RIGHT)
					degree = (float) (getInverseDegree(world.getGun2().getDegree()) + 90);
				else
					degree = (float) (world.getGun2().getDegree() - 90);
				double angleInRadian = Math.toRadians(degree);
				double sin = Math.sin(angleInRadian);
				double cos = Math.cos(angleInRadian);
				
				position.x = world.getGun2().getPosition().x;
				position.y = world.getGun2().getPosition().y;
				VySpeed = (float) (speed * cos);
				VxSpeed = (float) (speed * sin);
			}
		}
	}
	
	public void update(){
		fire();
		move();
		bulletHit();
	}
}
