package com.mygdx.gmgame;

import com.badlogic.gdx.math.Vector2;

public class Bullet {
	protected World world;
	protected Map map;
	protected Vector2 position;
	protected Player1 player1;
	protected Player2 player2;
	public boolean isVisible = true, isNull = false, fired = false;
	
	protected float speed = 30;
	public static float maxSpeed = 45;
	protected float VxSpeed = 0;
	protected float VySpeed = 0;
	protected double degree = 0;
	private float gravity = (float) 1;
	
	
	protected boolean isPressFire = false;
	
	public Bullet(World world) {
		this.world = world;
		this.player1 = world.getPlayer1();
		this.player2 = world.getPlayer2();
		position = new Vector2(0, 0);
	}
	
	public Vector2 getPosition(){
		return position;
	}
	
	protected void move() {
		if(isVisible) {
			position.y = position.y + VySpeed/2;
			VySpeed += gravity;
			position.x = position.x + VxSpeed;
		}
	}
	
	protected double getInverseDegree(double x){
		double result = x;
		for(int i = 1; i <= 360; i++){
			if(x + i == 360)
				result = i;
		}
		return result;
	}
	
	protected void bulletHit() {
		map = world.getMap();
    	if(world.map.hasWallAt(getRow(),getColumn())) {
    		world.map.deleteBlock(getRow(),getColumn());
    		isVisible = false;
    		isNull = true;
    		fired = false;
    	} else if(( getRow()==player1.getRow() || getRow()==player1.getRow()-1) && (getColumn()==player1.getColumn() || getColumn()==player1.getColumn()+1 )&& isVisible) {
    		Player1.life--;
    		isVisible = false;
    		isNull = true;
    		fired = false;
    	} else if(( getRow()==player2.getRow() || getRow()==player2.getRow()-1) && (getColumn()==player2.getColumn() || getColumn()==player2.getColumn()+1 ) && isVisible) {
    		Player2.life--;
    		isVisible = false;
    		isNull = true;
    		fired = false;
    	}
	}
	
	protected int getRow() {
	    return ((int)position.y) / WorldRenderer.BLOCK_SIZE; 
	}
	
	protected int getColumn() {
	    return ((int)position.x) / WorldRenderer.BLOCK_SIZE; 
	}
	
	public float getSpeed(){
		return speed;
	}
}