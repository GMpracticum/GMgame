package com.mygdx.gmgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen extends ScreenAdapter {
	private World world;
	private WorldRenderer worldRenderer;
	private MapRenderer mapRenderer;
	private PlayerRenderer playerRenderer;
	private GunRenderer gunRenderer;
	private BulletRenderer bulletRenderer;
	private StatusBarRenderer statusBarRenderer;

	
	public GameScreen(MyGame myGame) {
		world = new World(myGame);
		mapRenderer = new MapRenderer(world);
		playerRenderer = new PlayerRenderer(world);
		gunRenderer = new GunRenderer(world);
		bulletRenderer = new BulletRenderer(world);
		statusBarRenderer = new StatusBarRenderer(world);
		worldRenderer = new WorldRenderer(world, mapRenderer, playerRenderer, gunRenderer, bulletRenderer, statusBarRenderer);
	}
	
	public void render(float delta) {
    	update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        worldRenderer.render(delta);
	}
	
	public void update(float delta) {
		world.update(delta);
	}

}