package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StatusBarRenderer {
	private Texture player1Fired = new Texture("player1Fired.png");
	private Texture player2Fired = new Texture("player2Fired.png");
	private Texture GMgame = new Texture("GMgame.png");
	private Texture charge0 = new Texture("charge0n.png");
	private Texture charge1 = new Texture("charge1n.png");
	private Texture charge2 = new Texture("charge2n.png");
	private Texture charge3 = new Texture("charge3n.png");
	private Texture charge4 = new Texture("charge4n.png");
	private Texture charge5 = new Texture("charge5n.png");
	private Texture charge6 = new Texture("charge6n.png");
	private Texture charge7 = new Texture("charge7n.png");
	private Texture charge8 = new Texture("charge8n.png");
	private Texture charge9 = new Texture("charge9n.png");
	private Texture charge10 = new Texture("charge10.png");
	private World world;
	private SpriteBatch batch;
	
	public StatusBarRenderer(World world){
		this.world = world;
		this.batch = world.getMyGame().batch;
	}
	
	public void render(float delta){
		renderTurnBar();
		renderChargePlayer1();
		renderChargePlayer2();
	}
	
	private void renderTurnBar(){
		if(world.getStatusBar().playerTurn == StatusBar.PLAYER1)
			batch.draw(player1Fired, 0, 0);
		else if(world.getStatusBar().playerTurn == StatusBar.PLAYER2)
			batch.draw(player2Fired, 0, 0);
		else
			batch.draw(GMgame, 0, 0);
	}
	
	private void renderChargePlayer1(){
		if(world.getStatusBar().charge1 == 0)
			batch.draw(charge0, 200, 0);
		if(world.getStatusBar().charge1 == 1)
			batch.draw(charge1, 200, 0);
		if(world.getStatusBar().charge1 == 2)
			batch.draw(charge2, 200, 0);
		if(world.getStatusBar().charge1 == 3)
			batch.draw(charge3, 200, 0);
		if(world.getStatusBar().charge1 == 4)
			batch.draw(charge4, 200, 0);
		if(world.getStatusBar().charge1 == 5)
			batch.draw(charge5, 200, 0);
		if(world.getStatusBar().charge1 == 6)
			batch.draw(charge6, 200, 0);
		if(world.getStatusBar().charge1 == 7)
			batch.draw(charge7, 200, 0);
		if(world.getStatusBar().charge1 == 8)
			batch.draw(charge8, 200, 0);
		if(world.getStatusBar().charge1 == 9)
			batch.draw(charge9, 200, 0);
		if(world.getStatusBar().charge1 == 10)
			batch.draw(charge10, 200, 0);
			
	}
	
	private void renderChargePlayer2(){
		if(world.getStatusBar().charge2 == 0)
			batch.draw(charge0, 450, 0);
		if(world.getStatusBar().charge2 == 1)
			batch.draw(charge1, 450, 0);
		if(world.getStatusBar().charge2 == 2)
			batch.draw(charge2, 450, 0);
		if(world.getStatusBar().charge2 == 3)
			batch.draw(charge3, 450, 0);
		if(world.getStatusBar().charge2 == 4)
			batch.draw(charge4, 450, 0);
		if(world.getStatusBar().charge2 == 5)
			batch.draw(charge5, 450, 0);
		if(world.getStatusBar().charge2 == 6)
			batch.draw(charge6, 450, 0);
		if(world.getStatusBar().charge2 == 7)
			batch.draw(charge7, 450, 0);
		if(world.getStatusBar().charge2 == 8)
			batch.draw(charge8, 450, 0);
		if(world.getStatusBar().charge2 == 9)
			batch.draw(charge9, 450, 0);
		if(world.getStatusBar().charge2 == 10)
			batch.draw(charge10, 450, 0);
			
	}
}
