package com.mygdx.gmgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class Gun2 extends Gun {
	public void update(){
		if(Gdx.input.isKeyPressed(Keys.O)){
			increaseDegree(Gun.rotateSpeed);
    	}
		if(Gdx.input.isKeyPressed(Keys.P)){
			decreaseDegree(Gun.rotateSpeed);
    	}
	}
}