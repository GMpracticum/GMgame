package com.mygdx.gmgame;

public class Map {
	public StringBuilder[] MapData;
	private int height;
	private int width;
	
	public Map() {	
		MapData = new StringBuilder[32];
		MapData[0] = new StringBuilder("######################################################");
		MapData[1] = new StringBuilder("#................................................#####");
        MapData[2] = new StringBuilder("#................................................#####");
        MapData[3] = new StringBuilder("#................................................#####");
        MapData[4] = new StringBuilder("#................................................#####");
        MapData[5] = new StringBuilder("#................................................#####");
        MapData[6] = new StringBuilder("#..................#...........#.................#####");
        MapData[7] = new StringBuilder("#.................##...........##................#####");
		MapData[8] = new StringBuilder("#................###.....xxxx..###...............#####");
        MapData[9] = new StringBuilder("#...............####...........####..............#####");
        MapData[10] = new StringBuilder("#.............#####...........#####..............#####");
        MapData[11] = new StringBuilder("#............######xx.......xx######.............#####");
        MapData[12] = new StringBuilder("#.....#############...........#############......#####");
        MapData[13] = new StringBuilder("#...........................................xxxxx#####");
        MapData[14] = new StringBuilder("#xx..................xxxxxx.............xx.......#####");
		MapData[15] = new StringBuilder("#..xx............................................#####");
        MapData[16] = new StringBuilder("#.......xxx....xx........xx..............xx......#####");
        MapData[17] = new StringBuilder("#...................xx............xx.............#####");
        MapData[18] = new StringBuilder("#...........xx...................................#####");
        MapData[19] = new StringBuilder("#................................................#####");
        MapData[20] = new StringBuilder("#................................................#####");
        MapData[21] = new StringBuilder("#.....xxxxx............xx.....xx.......xxxxx.....#####");
		MapData[22] = new StringBuilder("#...................xx...........................#####");
        MapData[23] = new StringBuilder("#..........xxxxxxx..................xxx..........#####");
        MapData[24] = new StringBuilder("#......................xxxx......................#####");
        MapData[25] = new StringBuilder("#................................xxxxxxxxxx......#####");
        MapData[26] = new StringBuilder("#.....xxxxxxxxxxxx...............................#####");
        MapData[27] = new StringBuilder("#.......................xxxxxxxxxxx..............#####");
        MapData[28] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#####");
		MapData[29] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#####");
        MapData[30] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#####");
        MapData[31] = new StringBuilder("######################################################");
		height = MapData.length;
		width = MapData[0].length();
	}
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	public boolean hasWallAt( int r , int c) {
		return MapData[r].charAt(c) == '#' || MapData[r].charAt(c) == 'x';
	}
	
	public boolean onFloor(int r , int c) {
		return MapData[r+1].charAt(c) == '#' || MapData[r+1].charAt(c) == 'x';
	}
	
	public void deleteBlock(int r, int c) {
		if(MapData[r].charAt(c) == 'x') {
			MapData[r].setCharAt(c, '.');
		}
	}

}