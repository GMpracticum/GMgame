package com.mygdx.gmgame;

public class StatusBar {
	public static final int PLAYER1 = 11;
	public static final int PLAYER2 = 22;
	public int playerTurn = 0;
	public int charge1 = 0;
	public int charge2 = 0;
	
	private World world;
	
	public StatusBar(World world){
		this.world = world;
	}
	
	public void update(){
		updateCharge1();
		updateCharge2();
	}
	
	public void updateCharge1(){
		if(world.getBullet1().getSpeed() == 0)
			charge1 = 0;
		else if(world.getBullet1().getSpeed() <= 1*Bullet.maxSpeed/10)
			charge1 = 1;
		else if(world.getBullet1().getSpeed() <= 2*Bullet.maxSpeed/10)
			charge1 = 2;
		else if(world.getBullet1().getSpeed() <= 3*Bullet.maxSpeed/10)
			charge1 = 3;
		else if(world.getBullet1().getSpeed() <= 4*Bullet.maxSpeed/10)
			charge1 = 4;
		else if(world.getBullet1().getSpeed() <= 5*Bullet.maxSpeed/10)
			charge1 = 5;
		else if(world.getBullet1().getSpeed() <= 6*Bullet.maxSpeed/10)
			charge1 = 6;
		else if(world.getBullet1().getSpeed() <= 7*Bullet.maxSpeed/10)
			charge1 = 7;
		else if(world.getBullet1().getSpeed() <= 8*Bullet.maxSpeed/10)
			charge1 = 8;
		else if(world.getBullet1().getSpeed() <= 9*Bullet.maxSpeed/10)
			charge1 = 9;
		else if(world.getBullet1().getSpeed() <= 10*Bullet.maxSpeed/10)
			charge1 = 10;
	}
	
	public void updateCharge2(){
		if(world.getBullet2().getSpeed() == 0)
			charge2 = 0;
		else if(world.getBullet2().getSpeed() <= 1*Bullet.maxSpeed/10)
			charge2 = 1;
		else if(world.getBullet2().getSpeed() <= 2*Bullet.maxSpeed/10)
			charge2 = 2;
		else if(world.getBullet2().getSpeed() <= 3*Bullet.maxSpeed/10)
			charge2 = 3;
		else if(world.getBullet2().getSpeed() <= 4*Bullet.maxSpeed/10)
			charge2 = 4;
		else if(world.getBullet2().getSpeed() <= 5*Bullet.maxSpeed/10)
			charge2 = 5;
		else if(world.getBullet2().getSpeed() <= 6*Bullet.maxSpeed/10)
			charge2 = 6;
		else if(world.getBullet2().getSpeed() <= 7*Bullet.maxSpeed/10)
			charge2 = 7;
		else if(world.getBullet2().getSpeed() <= 8*Bullet.maxSpeed/10)
			charge2 = 8;
		else if(world.getBullet2().getSpeed() <= 9*Bullet.maxSpeed/10)
			charge2 = 9;
		else if(world.getBullet2().getSpeed() <= 10*Bullet.maxSpeed/10)
			charge2 = 10;
	}
}
