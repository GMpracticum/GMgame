package practicum;

import org.usb4java.Device;

public class TestUSBBoard {

	public static void main(String[] args) {
		McuBoard.initUsb();
		Device[] devs = McuBoard.findBoards();
		McuBoard b = new McuBoard(devs[0]);
		b.write((byte) 0, (short) 2, (short) 1);
		
		byte[] ret = b.read((byte) 2, (short) 0, (short) 0);
		
		if(ret[0] == 1){
			b.write((byte) 0, (short) 2, (short) 1);
			System.out.println("Switch is pressed");
		}
		else{
			System.out.println("Switch is not pressed");
			b.write((byte) 0, (short) 2, (short) 0);
		}
		

		b.write((byte) 0, (short) 2, (short) 0);
		b.write((byte) 0, (short) 1, (short) 1);
		b.write((byte) 0, (short) 0, (short) 1);
		
		McuBoard.cleanupUsb();
	}

}
